# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from random import *
# Create your models here.

class Person(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=254)