$(document).ready(function () {
   function setHeight() {
       var windowHeight = $(window).innerHeight;
       $('.sec-1').css('min-height', windowHeight);
       $('.sec-2').css('min-height', windowHeight);
       console.log('height changed')
   }

   setHeight();

   $(window).resize(function () {
       setHeight()
   })
});