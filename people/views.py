# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from email.mime.image import MIMEImage

from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from datblackdevspace.settings import DEFAULT_FROM_EMAIL, STATIC_ROOT
from .forms import SignUpForm
from .models import Person


# Create your views here.

def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST, auto_id=True)
        if form.is_valid():
            to = form.cleaned_data['email']

            html_content = render_to_string('email.html', {'first_name': form.data['first_name']})
            string = strip_tags(html_content)

            mail = EmailMultiAlternatives('Thanks for registering your interest', string, DEFAULT_FROM_EMAIL, [to])
            mail.attach_alternative(html_content, 'text/html')

            for f in ['fa-twitter.png', 'fa-instagram.png', 'fa-snapchat.png']:
                img_data = open(STATIC_ROOT + '/people/' + f, 'rb').read()
                img = MIMEImage(img_data)
                print('<%s>' % f)
                img.add_header('Content-ID', '<%s>' % f)
                img.add_header('Content-Disposition', 'inline', filename=f)
                mail.attach(img)

            mail.send()
            form.save()
        return render(request, 'thanks_page.html')
    else:
        form = SignUpForm()

    return render(request, 'signup.html', {'form': form})


def get_person(request, person_id):
    p = get_object_or_404(Person, pk=person_id)
    response = "Hello, " + p.first_name
    return HttpResponse(response)
