from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Person

class SignUpForm(ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'First Name'}),max_length=100,label='')
    last_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Last Name'}),max_length=100,label='')
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email'}),max_length=254,label='')



    class Meta:
        model = Person
        fields = ( 'first_name','last_name', 'email')

    # this here checks to see if the email entered already exists in the system
    def clean_email(self):
        data = self.cleaned_data['email']

        try:
            # if the person exists, raise that error boy
            match = Person.objects.get(email=data)
            raise forms.ValidationError("This email already exists")
        except Person.DoesNotExist:
            # you already know what's going on.
            return data