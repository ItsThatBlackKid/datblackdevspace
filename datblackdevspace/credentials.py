SECRET_KEY = '$v^r83w-i+9wo+-*#$zc(r5*ot2wu3b6xwvgla551fu-bsq(96'

DEBUG = True

TEMPLATE_DEBUG = True

import os

ALLOWED_HOSTS = ['127.0.0.1', 'localhost', '.datblackdev.space', '10.0.0.3']
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_FILE_PATH = '/hdd/datblackdev_test/tmp'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'sheku@datblackdev.space'
EMAIL_HOST_PASSWORD = 'bigBlackSpace'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'Sheku Kanneh <sheku.kanneh@datblackdev.space>'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.normpath(os.path.join(BASE_DIR, 'people/static')),
]

STATIC_ROOT = os.path.normpath(os.path.join(BASE_DIR, 'staticfiles'))
print(BASE_DIR + STATIC_ROOT)
